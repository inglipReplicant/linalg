use crate::error::MathError;
use crate::vector::Vector;
use crate::PRECISION;

use std::fmt;

type Result<T> = std::result::Result<T, MathError>;

pub trait Matrix {
    fn add(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn sub(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn mul(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn transpose(&self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn from_vec_into_col(v: &impl Vector) -> Self;

    fn from_vec_into_row(v: &impl Vector) -> Self;

    fn values(&self) -> &Vec<Vec<f64>>;

    fn values_mut(&mut self) -> &mut Vec<Vec<f64>>;

    fn dim_cols(&self) -> usize;

    fn dim_rows(&self) -> usize;

    fn get_elem(&self, row: usize, col: usize) -> Option<f64>;

    fn get_row(&self, index: usize) -> Option<&Vec<f64>>;

    fn get_col(&self, index: usize) -> Option<Vec<f64>>;
}

#[derive(Debug)]
pub struct RealMat {
    vals: Vec<Vec<f64>>,

    rows: usize,

    cols: usize,
}

impl RealMat {
    pub fn from_vec(v: Vec<Vec<f64>>) -> Result<RealMat> {
        let rs = v.len();
        let cs = v[0].len();

        for r in v.iter() {
            if r.len() != cs {
                return Err(MathError::DimensionError);
            }
        }

        let mat = RealMat {
            vals: v,
            rows: rs,
            cols: cs,
        };
        Ok(mat)
    }

    fn check_dims_add(&self, other: &RealMat) -> Result<()> {
        if self.cols != other.dim_cols() || self.rows != other.dim_rows() {
            return Err(MathError::DimensionError);
        }
        Ok(())
    }

    fn check_dims_mul(&self, other: &RealMat) -> Result<()> {
        if self.cols == other.rows {
            return Ok(());
        }
        Err(MathError::DimensionError)
    }
}

impl PartialEq for RealMat {
    fn eq(&self, other: &RealMat) -> bool {
        if self.check_dims_add(other).is_err() {
            return false;
        }
        let nrows = self.dim_rows();
        let ncols = self.dim_cols();

        for i in 0..nrows {
            for j in 0..ncols {
                let a = self.get_elem(i, j).unwrap();
                let b = self.get_elem(i, j).unwrap();
                if (a - b).abs() > PRECISION {
                    return false;
                }
            }
        }
        true
    }
}

impl Eq for RealMat {}

impl fmt::Display for RealMat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self.vals[..])
    }
}

impl Matrix for RealMat {

    fn values(&self) -> &Vec<Vec<f64>> {
        &self.vals
    }

    fn values_mut(&mut self) -> &mut Vec<Vec<f64>> {
        &mut self.vals
    }

    fn dim_cols(&self) -> usize {
        self.cols
    }

    fn dim_rows(&self) -> usize {
        self.rows
    }

    fn get_elem(&self, row: usize, col: usize) -> Option<f64> {
        Some(self.vals.get(row)?.get(col)?.clone())
    }

    fn get_row(&self, index: usize) -> Option<&Vec<f64>> {
        Some(self.vals.get(index)?)
    }

    fn get_col(&self, index: usize) -> Option<Vec<f64>> {
        if index > self.cols {
            return None;
        }
        let mut col = Vec::with_capacity(self.rows);
        for r in self.vals.iter() {
            col.push(r.get(index)?.clone());
        }
        Some(col)
    }

    /// Adds two matrices if their dimensions are compatible.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 3.0, 4.0],
    ///         vec![5.0, 6.0, 7.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![3.0, 5.0, 7.0],
    ///         vec![9.0, 11.0, 13.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let sum = a.add(&b).unwrap();
    /// assert_eq!(sum, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn add(&self, other: &RealMat) -> Result<RealMat> {
        self.check_dims_add(other)?;
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();
        let mut res_rows = Vec::new();

        for i in 0..n_rows {
            let mut row = Vec::new();
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() + other.get_elem(i, j).unwrap();
                row.push(elem);
            }
            res_rows.push(row);
        }
        RealMat::from_vec(res_rows)
    }

    /// Subtracts two matrices if their dimensions are compatible.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![2.0, 3.0, 4.0],
    ///         vec![5.0, 6.0, 7.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![-1.0, -1.0, -1.0],
    ///         vec![-1.0, -1.0, -1.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let sum = a.sub(&b).unwrap();
    /// assert_eq!(sum, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn sub(&self, other: &RealMat) -> Result<RealMat> {
        self.check_dims_add(other)?;
        let n_rows = self.dim_rows();
        let n_cols = self.dim_cols();
        let mut res_rows = Vec::new();

        for i in 0..n_rows {
            let mut row = Vec::new();
            for j in 0..n_cols {
                let elem = self.get_elem(i, j).unwrap() - other.get_elem(i, j).unwrap();
                row.push(elem);
            }
            res_rows.push(row);
        }
        RealMat::from_vec(res_rows)
    }

    /// Multiplies two matrices if their dimensions are compatible.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let b = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 4.0],
    ///         vec![2.0, 5.0],
    ///         vec![3.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![14.0, 32.0],
    ///         vec![32.0, 77.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let prod = a.mul(&b).unwrap();
    /// assert!(b.mul(&a).is_ok());
    ///
    /// assert_eq!(prod.get_elem(0, 0).unwrap(), expected.get_elem(0, 0).unwrap());
    /// assert_eq!(prod.get_elem(0, 1).unwrap(), expected.get_elem(0, 1).unwrap());
    /// assert_eq!(prod.get_elem(1, 0).unwrap(), expected.get_elem(1, 0).unwrap());
    /// assert_eq!(prod.get_elem(1, 1).unwrap(), expected.get_elem(1, 1).unwrap());
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix dimensions are not compatible.
    fn mul(&self, other: &RealMat) -> Result<RealMat> {
        self.check_dims_mul(other)?;
        let n = self.rows;
        let p = other.dim_cols();
        let m = self.cols;
        let mut res = Vec::with_capacity(n);

        for i in 0..n {
            let mut c = Vec::with_capacity(p);
            for j in 0..p {
                let mut sum = 0.0;
                for k in 0..m {
                    sum += self.get_elem(i, k).unwrap() * other.get_elem(k, j).unwrap();
                }
                c.push(sum);
            }
            res.push(c);
        }
        RealMat::from_vec(res)
    }

    /// Transposes a matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::matrix::Matrix;
    /// use linalg::matrix::RealMat;
    ///
    /// let a = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 2.0, 3.0],
    ///         vec![4.0, 5.0, 6.0]
    ///     ]
    /// ).unwrap();
    /// let expected = RealMat::from_vec(
    ///     vec![
    ///         vec![1.0, 4.0],
    ///         vec![2.0, 5.0],
    ///         vec![3.0, 6.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let t = a.transpose().unwrap();
    /// assert_eq!(t, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if matrix creation fails, which should be never.
    fn transpose(&self) -> Result<RealMat> {
        let n_cols = self.dim_rows();
        let n_rows = self.dim_cols();
        let mut rows = Vec::with_capacity(n_rows);

        for i in 0..n_rows {
            let mut row = Vec::with_capacity(n_cols);
            for j in 0..n_cols {
                let elem = self.get_elem(j, i).unwrap();
                row.push(elem.clone());
            }
            rows.push(row);
        }
        RealMat::from_vec(rows)
    }

    /// Creates a row matrix from a vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::{Vector, RealVec};
    /// use linalg::matrix::{Matrix, RealMat};
    ///
    /// let a = RealVec::from_vec(vec![12.0, 20.0, 8.0, 8.0]);
    /// let m = RealMat::from_vec_into_col(&a);
    ///
    /// assert_eq!(m.dim_cols(), 1);
    /// assert_eq!(m.dim_rows(), 4);
    /// ```
    fn from_vec_into_col(v: &impl Vector) -> RealMat {
        let vals = v.elems();
        let mut rows = Vec::with_capacity(vals.len());
        for item in vals.iter() {
            rows.push(vec![item.clone()]);
        }
        RealMat::from_vec(rows).unwrap()
    }

    /// Creates a row matrix from a vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::{Vector, RealVec};
    /// use linalg::matrix::{Matrix, RealMat};
    ///
    /// let a = RealVec::from_vec(vec![12.0, 20.0, 8.0, 8.0]);
    /// let m = RealMat::from_vec_into_row(&a);
    ///
    /// assert_eq!(m.dim_rows(), 1);
    /// assert_eq!(m.dim_cols(), 4);
    /// ```
    fn from_vec_into_row(v: &impl Vector) -> RealMat {
        let cols = v.elems().iter().cloned().collect::<Vec<f64>>();
        let mut row = Vec::new();
        row.push(cols);
        RealMat::from_vec(row).unwrap()
    }
}
