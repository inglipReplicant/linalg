pub mod error;
pub mod matrix;
pub mod vector;

pub static PRECISION: f64 = 1e-6; //floating point precision for float comparison